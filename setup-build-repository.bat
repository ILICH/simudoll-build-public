@echo off

:: install chocolatey
powershell -noProfile -executionPolicy bypass -command "& {iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex }"
SET PATH=%PATH%;C:\ProgramData\chocolatey\bin

:: install git
choco install git -y
SET PATH=%PATH%;C:\Program Files\Git\cmd

:: clone build-public repository
git clone -b master https://bitbucket.org/ILICH/simudoll-build-public.git build-public

:: clone build repository
git clone -b master https://bitbucket.org/ILICH/simudoll-build.git build

:: start script which configures environment
call build\setup-environment.bat

:: make sure that we are not closing terminal window immediately, so that developer can see the 'success' message 
pause

:: delete this script, as we have it now in a build-public folder
(goto) 2>nul & del "%~f0"